import {ApiService} from './api.service';
import {Injectable} from '@angular/core';

@Injectable()
export class UserService extends ApiService {
  getEndpoint(): string {
    return '/users';
  }
}
