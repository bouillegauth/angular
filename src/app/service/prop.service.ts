import {ApiService} from './api.service';
import {Injectable} from '@angular/core';

@Injectable()
export class UserPropService extends ApiService {
  getEndpoint(): string {
    return '/user_props';
  }
}
