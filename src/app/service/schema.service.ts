import {ApiService} from './api.service';
import {Injectable} from '@angular/core';

@Injectable()
export class SchemaService extends ApiService {
  getEndpoint(): string {
    return '/user_schemas';
  }
}
