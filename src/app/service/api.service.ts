import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export abstract class ApiService {
  constructor(private httpClient: HttpClient) {
  }

  abstract getEndpoint(): string;

  protected getPath(route: string): string {
    const host = environment.api_host;

    return `${host}${this.getEndpoint()}${route}`;
  }

  public getAll(): Observable<any> {
    return this.get('');
  }

  public getById(id: string): Observable<any> {
    return this.get(`/${id}`);
  }

  public create(data: any): Observable<any> {
    return this.post('', data);
  }

  public deleteById(id: string): Observable<any> {
    return this.delete(`/${id}`);
  }

  protected get(url: string): Observable<any> {
    url = this.getPath(url);

    return this.httpClient.get(url);
  }

  protected post(url: string, data: any): Observable<any> {
    url = this.getPath(url);

    return this.httpClient.post(url, data);
  }

  protected delete(url: string): Observable<any> {
    url = this.getPath(url);

    return this.httpClient.delete(url);
  }
}
