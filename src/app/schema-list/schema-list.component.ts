import {Component, OnInit} from '@angular/core';
import {SchemaService} from '../service/schema.service';
import {ApiService} from '../service/api.service';
import {GenericListComponent} from '../generic-list/generic-list.component';

@Component({
  selector: 'app-schema-list',
  templateUrl: './schema-list.component.html',
  styleUrls: ['./schema-list.component.scss']
})
export class SchemaListComponent extends GenericListComponent {
  public columns = [{
    title: 'ID',
    key: 'id',
    ignoreInForm: true
  }, {
    title: 'Key',
    key: 'key'
  }, {
    title: 'Description',
    key: 'description'
  }];

  public enabledOperations = {
    edit: true,
    delete: true
  };

  constructor(private schemaService: SchemaService) {
    super();
  }

  protected getDataService(): ApiService {
    return this.schemaService;
  }

  protected resetElementToCreate(): void {
    this.elementToCreate = {
      key: 'Name of the key',
      description: 'Name of the description'
    };
  }
}
