import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UserListComponent} from './user-list/user-list.component';
import {SchemaListComponent} from './schema-list/schema-list.component';
import {SchemaService} from './service/schema.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {UserService} from './service/user.service';
import {UserPropService} from './service/prop.service';
import {LoadingComponent} from './loading/loading.component';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    SchemaListComponent,
    LoadingComponent
  ],
  exports: [
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    SchemaService,
    UserService,
    UserPropService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
