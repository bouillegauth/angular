import {Component, OnInit} from '@angular/core';
import {Routes} from '@angular/router';
import {routes} from './constants/routes.constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public routes: Routes = [];

  ngOnInit(): void {
    this.routes = routes;
  }
}
