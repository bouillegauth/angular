import {Component, OnInit} from '@angular/core';
import {GenericListComponent} from '../generic-list/generic-list.component';
import {UserService} from '../service/user.service';
import {ApiService} from '../service/api.service';
import {SchemaService} from '../service/schema.service';
import {map} from 'rxjs/operators';
import {forkJoin} from 'rxjs';
import {UserPropService} from '../service/prop.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent extends GenericListComponent implements OnInit {
  public columns = [{
    title: 'ID',
    key: 'id',
    ignoreInForm: true
  }, {
    title: 'First Name',
    key: 'firstName'
  }, {
    title: 'Last Name',
    key: 'lastName'
  }, {
    title: 'Email',
    key: 'email'
  }];

  public schemaItems = [];
  public elementExtraColumns: any = {};

  public enabledOperations = {
    edit: false,
    delete: true
  };

  constructor(private userService: UserService,
              private schemaService: SchemaService,
              private propsService: UserPropService) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.listenForSchemaChanges();
  }

  private listenForSchemaChanges(): void {
    this.schemaService.getAll().pipe(map((res) => res['hydra:member'])).subscribe(
      (res) => {
        for (const r of res) {
          this.elementExtraColumns[r.key] = r.description;
        }

        this.schemaItems = res;
      }
    );
  }

  createElement(): void {
    super.createElement();

    this.itemCreatedEvent.asObservable().subscribe(
      (res) => {
        const obs = [];

        for (const schema of this.schemaItems) {
          const data = {
            user: res['@id'],
            key: schema['@id'],
            value: this.elementExtraColumns[schema.key]
          };

          obs.push(this.propsService.create(data));
        }

        forkJoin(obs).subscribe(
          () => {
            this.ngOnInit();
          }
        );
      }
    );
  }

  protected getDataService(): ApiService {
    return this.userService;
  }

  protected resetElementToCreate(): void {
    this.elementToCreate = {
      firstName: 'John',
      lastName: 'Smith',
      email: 'john@smith.com'
    };
  }
}
