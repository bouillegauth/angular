import {OnInit} from '@angular/core';
import {ApiService} from '../service/api.service';
import {map} from 'rxjs/operators';
import {Subject} from 'rxjs';

export abstract class GenericListComponent implements OnInit {
  public elementToCreate: any = {};
  public items = [];
  public loading = true;
  public columns = [];
  public enabledOperations = {
    edit: false,
    delete: false
  };
  public itemCreatedEvent = new Subject<boolean>();

  protected abstract getDataService(): ApiService;

  protected abstract resetElementToCreate(): void;

  ngOnInit() {
    this.resetElementToCreate();
    this.listenForDataChanges();
  }

  protected listenForDataChanges(): void {
    this.loading = true;

    this.getDataService().getAll().pipe(map((res) => res['hydra:member'])).subscribe(
      (res) => {
        this.items = res;
        this.loading = false;
      }
    );
  }

  public createElement(): void {
    this.getDataService().create(this.elementToCreate).subscribe(
      (res) => {
        this.resetElementToCreate();
        this.listenForDataChanges();

        this.itemCreatedEvent.next(res);
        this.itemCreatedEvent.complete();
      }
    );
  }

  public reactToDeleteEvent(schema: any): void {
    const approval = confirm(`Are you sure you want to remove the element with the ID ${schema['@id']}?`);

    if (false === approval) {
      return;
    }

    this.getDataService().deleteById(schema.id).subscribe(
      () => {
        this.listenForDataChanges();
      }
    );
  }
}
