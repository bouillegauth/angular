import {Routes} from '@angular/router';
import {SchemaListComponent} from '../schema-list/schema-list.component';
import {UserListComponent} from '../user-list/user-list.component';

export const routes: Routes = [
  {
    path: 'schema',
    component: SchemaListComponent,
    data: {
      title: 'Schema'
    }
  },
  {
    path: 'user/list',
    component: UserListComponent,
    data: {
      title: 'Users'
    }
  },
  {
    path: '**', redirectTo: 'user/list', data: []
  }
];
